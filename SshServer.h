#ifndef _SSHSERVER_H_
#define _SSHSERVER_H_

#include "Server.h"
#include "ClientSocket.h"

#include <libssh/libssh.h>
#include <libssh/server.h>
#include <string>

#define SSH_LISTEN_ERROR 10
#define SSH_ACCEPT_ERROR 11

//-----
// Implements simple SSH server using libssh library
// Inspired by https://github.com/PeteMo/sshpot
//-----

class SshServer : public Server
{
	private:
		ssh_session session;
		ssh_bind sshbind;

	public:
		SshServer(
			std::string address,
			unsigned short int port,
			unsigned int maxClientCount,
			std::string rsaFile);
		~SshServer();
		void startListen(void (*handler)(ssh_session));
};

#endif

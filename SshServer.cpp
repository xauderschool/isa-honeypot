#include "SshServer.h"

#include <thread>
#include <cstring>
#include <iostream>

//-----
// Intializes and configures SSH session
//-----

SshServer::SshServer(
	std::string address, 
	unsigned short int port, 
	unsigned int maxClientCount,
	std::string rsaFile) :
Server(maxClientCount)
{
   this->sshbind = ssh_bind_new();
   ssh_bind_options_set(this->sshbind, SSH_BIND_OPTIONS_BINDADDR, address.c_str());
   ssh_bind_options_set(this->sshbind, SSH_BIND_OPTIONS_BINDPORT, &port);
   ssh_bind_options_set(this->sshbind, SSH_BIND_OPTIONS_RSAKEY, rsaFile.c_str());
}

//-----
// Starts listening to incoming SSH connections
// User can set a function which will handle each one of them in a separate
// thread
//-----

void SshServer::startListen(void (*handler)(ssh_session))
{
	if(ssh_bind_listen(sshbind) < 0)
		throw SSH_LISTEN_ERROR;

	while(true)
	{
		this->session = ssh_new();

		if(ssh_bind_accept(this->sshbind, this->session) == SSH_ERROR)
			throw SSH_ACCEPT_ERROR;

		this->incClientCount();

		if(this->clientCount > this->maxClientCount)
			continue;

		std::thread commThread(*handler, this->session);
		commThread.detach();
	}
}

//-----
// Terminates the SSH server, called on SIGINT
//-----

SshServer::~SshServer()
{
	ssh_disconnect(this->session);
  ssh_bind_free(this->sshbind);
  ssh_finalize();
}

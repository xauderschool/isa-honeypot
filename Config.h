#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <string>

#define CONFIG_ADDRESS_ERROR 20
#define CONFIG_PORT_ERROR 21
#define CONFIG_LOG_ERROR 22
#define CONFIG_MODE_ERROR 23
#define CONFIG_RSA_ERROR 24

//-----
// Honeypot modes
//-----

typedef enum { MODE_UNDEFINED, MODE_SSH, MODE_FTP } Mode;

//-----
// Stores honeypot configuration recieved as command line arguments
//-----

typedef struct 
{
	Mode mode;
	std::string logFile;
	std::string ipAddress;
	std::string rsaFile;
	unsigned short int port;
	unsigned int maxClients;
	unsigned int maxRetries;
} 
Config;

//-----
// Creates configuration from command line arguments
//-----

Config getConfigFromArguments(int argc, char ** argv);


#endif

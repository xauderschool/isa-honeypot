#ifndef _SERVER_H_
#define _SERVER_H_

#include "ClientSocket.h"

#include <libssh/libssh.h>
#include <mutex>

//-----
// Basic class for a network server
//
// Takes care of clien limit and forces each concrete server to implement
// startListen() method
//-----

class Server 
{
	protected:
		std::mutex clientCountMtx;
		unsigned int maxClientCount;
		unsigned int clientCount;

	public:
		Server(unsigned int maxClientCount);
		virtual ~Server() {};
		virtual void startListen(void (*handler)(ClientSocket *)) {};
		virtual void startListen(void (*handler)(ssh_session)) {};
		void decClientCount();

	protected:
		void incClientCount();
};	

#endif

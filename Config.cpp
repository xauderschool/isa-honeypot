#include "Config.h"

#include <unistd.h>

//-----
// Creates configuration from command line arguments
//-----

Config getConfigFromArguments(int argc, char ** argv)
{
	std::string argValue;

	Config result;
	result.mode = MODE_UNDEFINED;
	result.maxRetries = 3;
	result.maxClients = 10;
	result.port = 0;
	result.ipAddress = "";
	result.logFile = "";
	result.rsaFile = "";

	int c;

	while ((c = getopt (argc, argv, "m:a:p:l:c:r:t:")) != -1)
	{
		if(c == 'm')
		{
			argValue = optarg;
			if(argValue == "ftp") 
				result.mode = MODE_FTP;
			else if(argValue == "ssh")
				result.mode = MODE_SSH;
		}
		else if(c == 'a')
			result.ipAddress = optarg;
		else if(c == 'p')
			result.port = atoi(optarg);
		else if(c == 'l')
			result.logFile = optarg;
		else if(c == 'c')
			result.maxClients = atoi(optarg);
		else if(c == 'r')
			result.rsaFile = optarg;
		else if(c == 't')
			result.maxRetries = atoi(optarg);
	}

	if(result.ipAddress == "")
		throw CONFIG_ADDRESS_ERROR;

	if(result.port == 0)
		throw CONFIG_PORT_ERROR;

	if(result.logFile == "")
		throw CONFIG_LOG_ERROR;

	if(result.mode == MODE_UNDEFINED)
		throw CONFIG_MODE_ERROR;

	if(result.mode == MODE_SSH and result.rsaFile == "")
		throw CONFIG_RSA_ERROR;

	return result;
}

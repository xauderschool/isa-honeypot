#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <mutex>
#include <fstream>
#include <string>

//-----
// Data which will be written to the log file
//-----

typedef struct 
{
	std::string mode;
	std::string ipAddress;
	std::string user;
	std::string password;
} 
LogRecord;

//-----
// Takes care of logging of aquired client authentication information
// Also manages the log file access from different threads
//-----

class Logger
{
	private:
		std::ofstream logFile;
		std::mutex mtx;

	public:
		Logger(std::string filepath);
		~Logger();
		void write(LogRecord logRecord);

	private:
		std::string getCurrentDateTime();
};

#endif

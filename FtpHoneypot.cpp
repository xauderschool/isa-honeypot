#include "FtpHoneypot.h"
#include "Logger.h"

#include <iostream>
#include <sstream>
#include <string>

extern Logger * logger;

using std::cout;
using std::endl;

//-----
// Simulates FTP Server and stores username and password of every
// connected client.
//
// Accepts a socket for communication with the client as a parameter
//-----

void ftpHoneypot(ClientSocket * clientSocket)
{
	std::string messageFromClient;
	std::string command;

	LogRecord logRecord = { .mode = "FTP" };

	// Get client's IP address
	cout << "New client connected ..." << endl;
	logRecord.ipAddress = clientSocket->getAddress();
	cout << "Client address is " << logRecord.ipAddress << endl;

	// FTP welcome message
	clientSocket->sendMessage("220 Server Ready\n");

	// Communication cycle
	while(true)
	{
		// Parse the message from the client and get the FTP command
		// Exception throw means that the connection was closed by client
		try 
		{
			messageFromClient = clientSocket->recieveMessage();
		}
		catch(int e)
		{
			break;
		}

		std::stringstream commandParser(messageFromClient);
		commandParser >> command;
		
		// Aquire client's username and request password from the client
		if(command == "USER")
		{
			commandParser >> logRecord.user;
			cout << "Username: " << logRecord.user << endl;
			clientSocket->sendMessage("331 Password requested\n");
		}

		// Aquire client's password, save all information to a log file,
		// send a message about incorrect credentials and close the socket
		else if(command == "PASS")
		{
			commandParser >> logRecord.password;
			cout << "Password: " << logRecord.password << endl;
			logger->write(logRecord);
			clientSocket->sendMessage("530 Login Incorrect\n");
			break;
		}

		// FTP honeypot does not understand any other commands
		else
		{
			clientSocket->sendMessage("502 I do not understand\n");
		}
	}

	delete clientSocket;
}

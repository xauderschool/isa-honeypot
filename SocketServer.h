#ifndef _SOCKETSERVER_H_
#define _SOCKETSERVER_H_

#include "Server.h"
#include "ClientSocket.h"

#include <iostream>
#include <list>
#include <string>
#include <thread>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

//-----
// Simple integer exceptions thrown when there is some problem
// with socket initialization
//-----

#define SOCKET_WRONG_ADDRESS 0
#define SOCKET_CREATE_FAILED 1
#define SOCKET_BIND_FAILED 2
#define SOCKET_LISTEN_FAILED 3

//-----
// Implements raw socket TCP server
//
// User can set the address and port and a handler, which is a function
// which will process every successfuly connected client
//-----

class SocketServer : public Server
{
	private:
		int protocol;
		int welcomeSocket;
		std::list<ClientSocket *> clientSockets;
		struct sockaddr_in serverAddress;
		struct sockaddr_in6 serverAddress6;
		struct sockaddr_in clientAddress;
		struct sockaddr_in6 clientAddress6;

	public:
		SocketServer(std::string address, unsigned short int port, unsigned int maxClientCount);
		~SocketServer();
		void startListen(void (*handler)(ClientSocket *));

	private:
		void resolveAddress(std::string address, unsigned short int port);
		void createSocket();
};

#endif

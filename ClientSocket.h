#ifndef _CLIENTSOCKET_H_
#define _CLIENTSOCKET_H_

#include <string>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define CONNECTION_CLOSED 30
#define READ_BUFFER_SIZE 512

//-----
// C++ OOP wrapper for BSD Socket for communication with connected client
//----- 

class ClientSocket 
{
	private:
		int protocol;
		int cSocket;
		int readBufferSize;
		std::string address;

	public:
		ClientSocket(int cSocket, int protocol, sockaddr * socketAddress);
		~ClientSocket();
		std::string getAddress();
		std::string recieveMessage();
		void sendMessage(std::string message);
};

#endif

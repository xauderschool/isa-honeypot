#ifndef _SSHHONEYPOT_H_
#define _SSHHONEYPOT_H_

#include	<libssh/libssh.h>
#include	<libssh/server.h>

//-----
// SSH Honeypot function.
// Takes care of communcation with every successfully connected client
// and stores their username and password to the log file
//-----

void sshHoneypot(ssh_session session);

#endif

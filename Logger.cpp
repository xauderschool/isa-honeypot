#include "Logger.h"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <sstream>

//-----
// Constructor opens the log file for writing
//-----

Logger::Logger(std::string filepath)
{
	this->logFile.open(filepath);
}

//-----
// Destructor closes the log file
//-----

Logger::~Logger()
{
	this->logFile.close();
}

//-----
// Writes the log record to the file
// Uses mutex to prevent more threads writing to the log file at the same time
//-----

void Logger::write(LogRecord logRecord)
{
	this->mtx.lock();

	this->logFile << logRecord.mode << " "
	              << this->getCurrentDateTime() << " "
	              << logRecord.ipAddress << " "
	              << logRecord.user << " "
	              << logRecord.password << std::endl;

	this->mtx.unlock();
}

//-----
// Returns current date and time as a YYYY-MM-DD H:i:S string
//-----

std::string Logger::getCurrentDateTime()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::stringstream ss;
	ss << tm.tm_year + 1900 << "-" << tm.tm_mon << "-" << tm.tm_mday
	   << " " << tm.tm_hour << ":" << tm.tm_min << ":" << tm.tm_sec;
	return ss.str();
}

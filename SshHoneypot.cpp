#include "SshHoneypot.h"
#include "Logger.h"
#include "Server.h"

#include <iostream>
#include <arpa/inet.h>

extern Logger * logger;
extern Server * server;

using std::cout;
using std::endl;

//-----
// Helper function for extracting client IP address
//-----

std::string getClientIp(ssh_session session) {
    struct sockaddr_storage tmp;
    unsigned int len = 100;
    char ip[100] = "\0";

    getpeername(ssh_get_fd(session), (struct sockaddr*)&tmp, &len);

    if(tmp.ss_family == AF_INET6)
    {
    	struct sockaddr_in6 *sock;
    	sock = (struct sockaddr_in6 *)&tmp;
    	inet_ntop(AF_INET6, &sock->sin6_addr, ip, len);
    }
    else
    {
    	struct sockaddr_in *sock;
    	sock = (struct sockaddr_in *)&tmp;
    	inet_ntop(AF_INET, &sock->sin_addr, ip, len);
    }	

    std::string ip_str = ip;
    return ip_str;
}

//-----
// Simulates SSH Server and stores username and password of every
// connected client.
//
// Accepts a ssh_session created by libssh library
//-----

void sshHoneypot(ssh_session session)
{
	LogRecord logRecord = { .mode = "SSH" };
	ssh_message message;
	unsigned int loginAttempts;

	logRecord.ipAddress = getClientIp(session);

	// Handshake
	cout << "New client connected ..." << endl;
	cout << "IP Address: " << logRecord.ipAddress << endl;
	cout << "Exchanging keys ..." << endl;
	ssh_handle_key_exchange(session);

	while(true)
	{
		// Connection closed
		if((message = ssh_message_get(session)) == NULL)
			break;

		// Record data username and password send during a login attempt
		if(ssh_message_subtype(message) == SSH_AUTH_METHOD_PASSWORD)
		{
			cout << "Login attempt ..." << endl;
			loginAttempts++;
			logRecord.user = ssh_message_auth_user(message);
    	logRecord.password = ssh_message_auth_password(message);
    	cout << "User: " << logRecord.user << endl;
    	cout << "Password: " << logRecord.password << endl;
    	logger->write(logRecord);
		}

		// Default message for any other reply
		ssh_message_reply_default(message);
    ssh_message_free(message);
	}
	
	// Quit connection
	cout << "Client disconnected" << endl << endl;
	ssh_disconnect(session);
	server->decClientCount();
}

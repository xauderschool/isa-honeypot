#include "Server.h"

//-----
// Sets the maximum number of clients on the server at one time from the 
// parameter and client counter to zero
//-----

Server::Server(unsigned int maxClientCount)
{
	this->maxClientCount = maxClientCount;
	this->clientCount = 0;
}

//-----
// Allows to decrement the number of connected clients
//
// Can be called from the client connection thread after the connection is
// terminated
//-----

void Server::decClientCount()
{
	this->clientCountMtx.lock();
	this->clientCount--;
	this->clientCountMtx.unlock();
}

//-----
// Allows to increment the number of connected clients
//
// Should be called from startListen method after successful client connection
//-----

void Server::incClientCount()
{
	this->clientCountMtx.lock();
	this->clientCount++;
	this->clientCountMtx.unlock();
}

#include <signal.h>

#include "ClientSocket.h"
#include "Config.h"
#include "FtpHoneypot.h"
#include "Logger.h"
#include "Server.h"
#include "SocketServer.h"
#include "SshServer.h"
#include "SshHoneypot.h"

using std::cout;
using std::cerr;
using std::endl;

Server * server;
Logger * logger;
Config config;

//-----
// Quits the server on keyboard interrupt signal
// Also takes care of deallocating all dynamic memory
//-----
void sigint(int a)
{
	cout << endl 
	     << "Keyboard interrupt detected, stopping the server ..."
	     << endl;

	delete server;
	delete logger;
	exit(0);
}

//-----
// Main function starts the server based on the configuration
// recieved as command line arguments
//-----

int main(int argc, char ** argv)
{
	// Keyboard interrupt will stop the server
	signal(SIGINT, sigint);

	// Server does not crash if the clients terminates the connection
	signal(SIGPIPE, SIG_IGN);

	// Start the server
	try 
	{
		config = getConfigFromArguments(argc, argv);

		cout << "Opening log file ..." << endl;
		cout << "File: " << config.logFile << endl;
		logger = new Logger(config.logFile);

		cout << "Starting server ..." << endl;
		cout << "IP: " << config.ipAddress << endl;
		cout << "Port: " << config.port << endl;

		if(config.mode == MODE_FTP)
		{
			cout << 	"Mode: FTP" << endl; 
			server = new SocketServer(config.ipAddress, config.port, config.maxClients);
		}

		else
		{
			server = new SshServer(config.ipAddress, config.port, config.maxClients, config.rsaFile);
			cout << "Mode: SSH" << endl; 
		}

		cout << "Listening for incoming connections ..." << endl;

		if(config.mode == MODE_FTP)
			server->startListen(&ftpHoneypot);
		else
			server->startListen(&sshHoneypot);
	}

	// Write any error message on stderr
	catch(int e)
	{
		std::string errmsg;

		switch(e)
		{
			case SOCKET_WRONG_ADDRESS: 
				errmsg = "Could not parse socket address";
				break;
			case SOCKET_CREATE_FAILED: 
				errmsg = "Could not create new socket";
				break;
			case SOCKET_BIND_FAILED: 
				errmsg = "Could not bind the socket";
				break;
			case SOCKET_LISTEN_FAILED: 
				errmsg = "Could not start listening";
				break;
			case CONFIG_ADDRESS_ERROR: 
				errmsg = "No IP address provided";
				break;
			case CONFIG_PORT_ERROR: 
				errmsg = "No TCP port number provided";
				break;
			case CONFIG_LOG_ERROR: 
				errmsg = "No log file provided";
				break;
			case CONFIG_MODE_ERROR: 
				errmsg = "No mode selected";
				break;
			case CONFIG_RSA_ERROR: 
				errmsg = "No RSA key provided";
				break;
			case SSH_LISTEN_ERROR: 
				errmsg = "Could not start listening";
				break;
			case SSH_ACCEPT_ERROR: 
				errmsg = "Could not accept connection";
				break;
			default: 
				errmsg = "Unknown error";
				break;
		}
		
		cerr << "Error number " << e << " occured:" << endl
		     << errmsg << endl;
		return 1;
	}

	return 0;
}

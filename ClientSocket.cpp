#include "ClientSocket.h"
#include "Server.h"
#include <iostream>

extern Server * server;

//-----
// Stores static information about client connection socket
//-----

ClientSocket::ClientSocket(int cSocket, int protocol, sockaddr * socketAddress)
{
	this->cSocket = cSocket;
	this->protocol = protocol;

	char address[INET6_ADDRSTRLEN];

	if(protocol == AF_INET)
	{
		sockaddr_in * ipAddress = (sockaddr_in *) socketAddress;
		inet_ntop(AF_INET, &(ipAddress->sin_addr), address, INET_ADDRSTRLEN);
	}

	else
	{
		sockaddr_in6 * ip6Address = (sockaddr_in6 *) socketAddress;
		inet_ntop(AF_INET, &(ip6Address->sin6_addr), address, INET6_ADDRSTRLEN);
	}

	this->address = address;
}

//-----
// Correcty closes the BSD socket on deletion
//-----

ClientSocket::~ClientSocket()
{
	server->decClientCount();
	shutdown(this->cSocket, SHUT_RDWR);
	close(this->cSocket);
}

//-----
// Returns client IP addressa as a string
//-----

std::string ClientSocket::getAddress()
{
	return this->address;
}

//-----
// Recieves a message from a client in a form of a C++ string
//-----

std::string ClientSocket::recieveMessage()
{
	char buffer[READ_BUFFER_SIZE];
	int status = recv(this->cSocket, buffer, READ_BUFFER_SIZE, MSG_PEEK);
	if(status == 0) throw CONNECTION_CLOSED;
	std::string result = buffer;
	recv(this->cSocket, buffer, result.length(), 0);
	return result;
}

//-----
// Sends a message to a client
//-----

void ClientSocket::sendMessage(std::string message)
{
	const char * messageChars = message.c_str();
	int messageLength = message.length();
	send(this->cSocket, messageChars, messageLength, 0);
}

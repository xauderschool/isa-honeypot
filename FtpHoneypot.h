#ifndef _FTPHONEYPOT_H_
#define _FTPHONEYPOT_H_

#include "ClientSocket.h"

//-----
// FTP Honeypot function.
// Takes care of communcation with every successfully connected client
// and stores their username and password to the log file
//-----

void ftpHoneypot(ClientSocket * clientSocket);

#endif

CC = g++
CFLAGS = -Wall -std=c++11 -lpthread -lssh
EXECNAME = fakesrv

all: Main.o \
	   Server.o \
	   SocketServer.o \
	   SshServer.o \
	   ClientSocket.o \
	   Logger.o \
	   FtpHoneypot.o \
	   SshHoneypot.o \
	   Config.o
	$(CC) bin/*.o $(CFLAGS) -o $(EXECNAME)

%.o: %.cpp
	@mkdir bin 2>/dev/null || true
	@gcc -c $< -o bin/$@ $(CFLAGS)

clean:
	@rm -r bin
	@rm $(EXECNAME)

#include "SocketServer.h"

//-----
// Translates user readable address and port to inner representation usable by
// BSD sockets
//
// Also creates a socket for accepting new connections
//-----

SocketServer::SocketServer(
	std::string address, 
	unsigned short int port, 
	unsigned int maxClientCount) :
Server(maxClientCount)
{
	this->resolveAddress(address, port);
	this->createSocket();
}

//-----
// Closes all sockets before deleting the SocketServer object from memory
//-----

SocketServer::~SocketServer()
{
	this->clientSockets.clear();
	shutdown(this->welcomeSocket, SHUT_RDWR);
	close(this->welcomeSocket);
}

//-----
// Starts listening and acception incoming connections
// User can set a function which will handle each one of them in a separate
// thread
//-----

void SocketServer::startListen(void (*handler)(ClientSocket *))
{
	int result = listen(this->welcomeSocket, 1);
	if(result < 0) throw SOCKET_LISTEN_FAILED;

	socklen_t socketAddressSize;
	sockaddr * socketAddress;

	if(this->protocol == AF_INET6)
	{
		socketAddress = (sockaddr *) & this->clientAddress6;
		socketAddressSize = sizeof(this->clientAddress6);
	}

	else
	{
		socketAddress = (sockaddr *) & this->clientAddress;
		socketAddressSize = sizeof(this->clientAddress);
	}

	int commSocket;

	while(true)
	{
		commSocket = accept(
			this->welcomeSocket, socketAddress, &socketAddressSize);

		ClientSocket * clientSocket = new ClientSocket(
			commSocket, this->protocol, socketAddress);

		this->incClientCount();
		
		if(this->clientCount > this->maxClientCount)
		{
			delete clientSocket;
			continue;
		}

		this->clientSockets.push_back(clientSocket);
		std::thread commThread(*handler, clientSocket);
		commThread.detach();
	}
}

//-----
// Transforms IP address string and port number to an inner representation 
// usable by BSD sockets
//-----

void SocketServer::resolveAddress(std::string address, unsigned short int port)
{
	int result;
	const char * addressChars = address.c_str();

	result = inet_pton(AF_INET, addressChars, &(this->serverAddress.sin_addr));

	if(result == 1)
	{
		this->protocol = AF_INET;
		this->serverAddress.sin_family = AF_INET;
		this->serverAddress.sin_port = htons(port);
		return;
	}

	result = inet_pton(AF_INET6, address.c_str(), &(this->serverAddress6.sin6_addr));

	if(result)
	{
		this->protocol = AF_INET6;
		this->serverAddress6.sin6_family = AF_INET6;
		this->serverAddress6.sin6_port = htons(port);
		return;
	}

	throw SOCKET_WRONG_ADDRESS;
}

//-----
// Creates a socket which will be used to accept incomming connections
//-----

void SocketServer::createSocket()
{
	unsigned int socketAddressSize;
	sockaddr * socketAddress;

	this->welcomeSocket = socket(this->protocol, SOCK_STREAM, 0);
	int t = 1;
	setsockopt(this->welcomeSocket, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(int));
	
	if(this->protocol == AF_INET6)
	{
		socketAddress = (sockaddr *) & this->serverAddress6;
		socketAddressSize = sizeof(this->serverAddress6);
	}

	else
	{
		socketAddress = (sockaddr *) & this->serverAddress;
		socketAddressSize = sizeof(this->serverAddress);
	}

	if(this->welcomeSocket < 0) throw SOCKET_CREATE_FAILED;

	int result = bind(this->welcomeSocket, socketAddress, socketAddressSize);
	if(result < 0) throw SOCKET_BIND_FAILED;
}
